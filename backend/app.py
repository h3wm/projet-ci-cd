from flask import Flask, request, jsonify, session
import redis
import json
import uuid
from flask_cors import CORS
from werkzeug.utils import secure_filename
import os

app = Flask(__name__)
r = redis.Redis(host='localhost', port=6379, db=0, decode_responses=True)
UPLOAD_FOLDER = 'static/images'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = 'alalalalalalala'
print("coucou")
CORS(app, resources={r"/*": {"origins": "http://localhost:8000"}}, supports_credentials=True)


@app.route('/user', methods=['POST'])
def create_user():
    user_data = request.form.to_dict()
    image_file = request.files['image']

    if image_file:
        filename = secure_filename(image_file.filename)
        image_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        image_file.save(image_path)
        user_data['image'] = image_path

    user_id = str(uuid.uuid4())
    user_data['id'] = user_id 
    r.hmset(user_id, user_data)
    return jsonify(user_data), 201

@app.route('/users', methods=['GET'])
def get_all_users():
    keys = r.keys('*')
    users = [r.hgetall(key) for key in keys]
    return jsonify(users)

@app.route('/user/<user_id>', methods=['GET'])
def get_user(user_id):
    user = r.hgetall(user_id)
    if user:
        return jsonify(user)
    else:
        return jsonify({'error': 'Utilisateur non trouvé'}), 404

@app.route('/user/<user_id>', methods=['PUT'])
def update_user(user_id):
    updated_user = request.json
    if r.exists(user_id):
        r.hmset(user_id, updated_user)
        return jsonify({'message': 'Utilisateur mis à jour'}), 200
    else:
        return jsonify({'error': 'Utilisateur non trouvé'}), 404


@app.route('/user/<user_id>', methods=['DELETE'])
def delete_user(user_id):
    r.delete(user_id)
    return jsonify({'message': 'Utilisateur supprimé'}), 200

@app.route('/login', methods=['POST'])
def login():
    admin_username = 'admin'
    admin_password_hash = 'admin'
    username = request.json.get('username')
    password = request.json.get('password')

    if username == admin_username and admin_password_hash == password:
        session['logged_in'] = True
        return jsonify({'message': 'Connexion réussie'}), 200
    else:
        return jsonify({'message': 'Identifiants incorrects'}), 401

@app.route('/logout', methods=['POST'])
def logout():
    session.pop('admin_logged_in', None)
    return jsonify({'message': 'Déconnexion réussie'}), 200

@app.route('/check-login', methods=['GET'])
def check_login():
    if session.get('admin_logged_in'):
        return jsonify({'isLoggedIn': True})
    else:
        return jsonify({'isLoggedIn': False})

if __name__ == '__main__':
    app.run(debug=True)
