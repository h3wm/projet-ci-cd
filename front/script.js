let currentUser = null;
localStorage.setItem('isLoggedIn', 'false');

document.addEventListener('DOMContentLoaded', () => {
    fetchUsers();
});

function fetchUsers() {
    const stroboscope = document.getElementById('stroboscope');
    stroboscope.innerHTML = '';

    fetch('http://localhost:5000/users')
        .then(response => response.json())
        .then(users => {
            const stroboscope = document.getElementById('stroboscope');
            users.forEach(user => {
                const userContainer = document.createElement('div');
                userContainer.className = 'user-container';

                const img = document.createElement('img');
                img.src = user.image;
                img.alt = `Image de ${user.nom}`;

                const prenom = document.createElement('div');
                prenom.className = 'user-name';
                prenom.textContent = user.prenom;

                userContainer.appendChild(img);
                userContainer.appendChild(prenom);

                userContainer.onclick = () => showUserDetails(user);

                stroboscope.appendChild(userContainer);
            });
        });
}


function showUserDetails(user) {
    currentUser = user;
    console.log(currentUser);
    console.log('Affichage des détails pour :', user.nom);
    console.log('État de connexion admin:', localStorage.getItem('isLoggedIn'));
    const popup = document.getElementById('userPopup');
    let adminContent = '';

    if (localStorage.getItem('isLoggedIn') === 'true') {
        adminContent = `<span class="edit-icon" onclick="editUserDetails()">✏️</span>
                        <span class="delete-icon" onclick="confirmDeletion('${user.id}')">🗑️</span>`;
    }    

    popup.innerHTML = `
        <div>
            ${adminContent}
            <span class="close-btn" onclick="closePopup()">×</span>
            <img src="${user.image}" alt="Image de ${user.nom}" style="max-width: 80%; height: auto; border-radius: 10px; margin-bottom: 15px;">
            <h3>${user.nom} ${user.prenom}</h3>
            <p>Âge: ${user.age}</p>
            <p>Fonction: ${user.fonction}</p>
        </div>
    `;
    popup.style.display = 'block';
}

function editUserDetails() {
    if (currentUser) {
    user = currentUser;
    const popup = document.getElementById('userPopup');
    console.log(user.id);
    popup.innerHTML = `
        <div>
            <span class="close-btn" onclick="closePopup()">×</span>
            <input id="editImage" type="hidden" value="${user.image}">
            <input id="editNom" value="${user.nom}">
            <input id="editPrenom" value="${user.prenom}">
            <input id="editAge" type="number" value="${user.age}">
            <input id="editFonction" value="${user.fonction}">
            <button onclick="updateUserDetails('${user.id}')">Mettre à jour</button>
        </div>
    `;
    }
}

function updateUserDetails(userId) {
    console.log(userId);
    const updatedUser = {
        nom: document.getElementById('editNom').value,
        prenom: document.getElementById('editPrenom').value,
        age: document.getElementById('editAge').value,
        fonction: document.getElementById('editFonction').value,
        image: document.getElementById('editImage').value,
    };

    fetch(`http://localhost:5000/user/${userId}`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(updatedUser)
    })
    .then(response => response.json())
    .then(data => {
        console.log('Mise à jour réussie:', data);
        closePopup();
        fetchUsers();
    })
    .catch(error => {
        console.error('Erreur lors de la mise à jour:', error);
    });
}

function confirmDeletion(userId) {
    const isConfirmed = confirm("Êtes-vous sûr de vouloir supprimer cet utilisateur ?");
    if (isConfirmed) {
        deleteUser(userId);
    }
}

function deleteUser(userId) {
    fetch(`http://localhost:5000/user/${userId}`, {
        method: 'DELETE',
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);
        closePopup();
        fetchUsers();
    })
    .catch(error => {
        console.error('Erreur lors de la suppression:', error);
    });
}



function closePopup() {
    document.getElementById('addUserForm').style.display = 'none';
    const popup = document.getElementById('userPopup');
    popup.style.display = 'none';
}

function showAddUserForm() {
    document.getElementById('userForm').reset();
    document.getElementById('addUserForm').style.display = 'block';
}

document.getElementById('userForm').addEventListener('submit', function(e) {
    e.preventDefault();

    const nom = document.getElementById('nom').value;
    const prenom = document.getElementById('prenom').value;
    const age = document.getElementById('age').value;
    const fonction = document.getElementById('fonction').value;
    const image = document.getElementById('image').files[0];

    const userData = { nom, prenom, age, fonction };

    uploadImageToServer(image, userData)
        .then(data => {
            console.log('Utilisateur créé:', data);
            closePopup();
            fetchUsers(); 
        })
        .catch(error => {
            console.error('Erreur:', error);
        });
});


function uploadImageToServer(image, userData) {
    const formData = new FormData();
    formData.append('image', image);
    for (const key in userData) {
        formData.append(key, userData[key]);
    }

    return fetch('http://localhost:5000/user', {
        method: 'POST',
        body: formData
    })
    .then(response => response.json());
}

function checkLoginStatus() {
    fetch('http://localhost:5000/check-login', { credentials: 'include' })
        .then(response => response.json())
        .then(data => {
            if (data.isLoggedIn) {
                showAdminFeatures();
            } else {
                hideAdminFeatures();
            }
        });
}

function login(username, password) {
    fetch('http://localhost:5000/login', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password }),
        credentials: 'include'
    })
    .then(response => response.json())
    .then(data => {
        console.log(data.message);
        localStorage.setItem('isLoggedIn', 'true');
        showAdminFeatures();
    })
    .catch(error => {
        console.error('Erreur:', error);
    });
}

function logout() {
    fetch('http://localhost:5000/logout', {
        method: 'POST',
        credentials: 'include'
    })
    .then(response => response.json())
    .then(data => {
        console.log(data.message);
        hideAdminFeatures();
    })
    .catch(error => {
        console.error('Erreur:', error);
    });
}

function showAdminFeatures() {
    document.getElementById('adminFeatures').style.display = 'block';
    document.getElementById('adminLoginBtn').style.display = 'none';
}

function hideAdminFeatures() {
    document.getElementById('adminFeatures').style.display = 'none';
    document.getElementById('adminLoginBtn').style.display = 'block';
}

function showLoginForm() {
    const username = prompt("Nom d'utilisateur Admin :");
    const password = prompt("Mot de Passe Admin :");
    login(username, password);
}

